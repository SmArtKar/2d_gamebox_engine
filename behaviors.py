import tkinter
import PIL

class Behavior():
    def __init__(self, args):
        self.args = args

class BasicMove(Behavior):
    def active(self, parent, event):
        if event == None:
            pass
        else:
            if event.keysym == self.args["up"]:
                parent.args["y"] -= self.args["speed_y"]
            if event.keysym == self.args["down"]:
                parent.args["y"] += self.args["speed_y"]
            if event.keysym == self.args["left"]:
                parent.args["x"] -= self.args["speed_x"]
            if event.keysym == self.args["right"]:
                parent.args["x"] += self.args["speed_x"]

        for object in parent.args["all_sprites"]:
            if object.args["x"] + object.args["width"] > parent.args["x"] + parent.args["width"] and object.args["x"] < parent.args["x"] and object.args["y"] + object.args["height"] > parent.args["y"] + parent.args["height"] and object.args["y"] < parent.args["y"]:
                parent.args["x"] -= self.args["speed_x"] * 2
                parent.args["y"] -= self.args["speed_y"] * 2

            if object.args["x"] < parent.args["x"] + parent.args["width"] and object.args["x"] > parent.args["x"] and object.args["y"] < parent.args["y"] + parent.args["height"] and object.args["y"] > parent.args["y"]:
                parent.args["x"] -= self.args["speed_x"] * 2
                parent.args["y"] -= self.args["speed_y"] * 2

            if parent.args["x"] < object.args["x"] + object.args["width"] and parent.args["x"] > object.args["x"] and parent.args["y"] < object.args["y"] + object.args["height"] and parent.args["y"] > object.args["y"]:
                parent.args["x"] -= self.args["speed_x"] * -2
                parent.args["y"] -= self.args["speed_y"] * -2
            if object.args["x"] < parent.args["x"] + parent.args["width"] and object.args["x"] > parent.args["x"] and parent.args["y"] < object.args["y"] + object.args["height"] and parent.args["y"] > object.args["y"]:
                parent.args["x"] -= self.args["speed_x"] * 2
                parent.args["y"] -= self.args["speed_y"] * -2

            if parent.args["x"] < object.args["x"] + object.args["width"] and parent.args["x"] > object.args["x"] and object.args["y"] < parent.args["y"] + parent.args["height"] and object.args["y"] > parent.args["y"]:
                parent.args["x"] -= self.args["speed_x"] * -2
                parent.args["y"] -= self.args["speed_y"] * 2
        return parent

class AdvancedMove(Behavior):
    def active(self, parent, event):
        if event == None:
            pass
        else:
            if event.keysym == self.args["up"]:
                parent.args["speed_y"] += self.args["speed_y"]
            if event.keysym == self.args["down"]:
                parent.args["speed_y"] -= self.args["speed_y"]
            if event.keysym == self.args["left"]:
                parent.args["speed_x"] += self.args["speed_x"]
            if event.keysym == self.args["right"]:
                parent.args["speed_x"] -= self.args["speed_x"]
        parent.args["x"] -= parent.args["speed_x"]
        parent.args["y"] -= parent.args["speed_y"]
        if parent.args["speed_x"] > 0:
            parent.args["speed_x"] -= self.args["friction"]
        if parent.args["speed_x"] < 0:
            parent.args["speed_x"] += self.args["friction"]
        if parent.args["speed_y"] > 0:
            parent.args["speed_y"] -= self.args["friction"]
        if parent.args["speed_y"] < 0:
            parent.args["speed_y"] += self.args["friction"]

        if parent.args["speed_x"] > self.args["speed_x_max"]:
            parent.args["speed_x"] = self.args["speed_x_max"]
        if parent.args["speed_x"] < self.args["speed_x_max"]*-1:
            parent.args["speed_x"] = self.args["speed_x_max"]*-1
        if parent.args["speed_y"] > self.args["speed_y_max"]:
            parent.args["speed_y"] = self.args["speed_y_max"]
        if parent.args["speed_y"] < self.args["speed_y_max"]*-1:
            parent.args["speed_y"] = self.args["speed_y_max"]*-1

        for object in parent.args["all_sprites"]:

            if object.args["x"] + object.args["width"] > parent.args["x"] + parent.args["width"] and object.args["x"] < parent.args["x"] and object.args["y"] + object.args["height"] > parent.args["y"] + parent.args["height"] and object.args["y"] < parent.args["y"]:
                parent.args["speed_x"] = 1
                parent.args["speed_y"] = 1
                parent.args["x"] -= self.args["speed_x"] * 2
                parent.args["y"] -= self.args["speed_y"] * 2

            if object.args["x"] < parent.args["x"] + parent.args["width"] and object.args["x"] > parent.args["x"] and object.args["y"] < parent.args["y"] + parent.args["height"] and object.args["y"] > parent.args["y"]:
                parent.args["speed_x"] = 1
                parent.args["speed_y"] = 1
                parent.args["x"] -= self.args["speed_x"] * 2
                parent.args["y"] -= self.args["speed_y"] * 2

            if parent.args["x"] < object.args["x"] + object.args["width"] and parent.args["x"] > object.args["x"] and parent.args["y"] < object.args["y"] + object.args["height"] and parent.args["y"] > object.args["y"]:
                parent.args["speed_x"] = -1
                parent.args["speed_y"] = -1
                parent.args["x"] -= self.args["speed_x"] * -2
                parent.args["y"] -= self.args["speed_y"] * -2
            if object.args["x"] < parent.args["x"] + parent.args["width"] and object.args["x"] > parent.args["x"] and parent.args["y"] < object.args["y"] + object.args["height"] and parent.args["y"] > object.args["y"]:
                parent.args["speed_x"] = 1
                parent.args["speed_y"] = -1
                parent.args["x"] -= self.args["speed_x"] * 2
                parent.args["y"] -= self.args["speed_y"] * -2

            if parent.args["x"] < object.args["x"] + object.args["width"] and parent.args["x"] > object.args["x"] and object.args["y"] < parent.args["y"] + parent.args["height"] and object.args["y"] > parent.args["y"]:
                parent.args["speed_x"] = -1
                parent.args["speed_y"] = 1
                parent.args["x"] -= self.args["speed_x"] * -2
                parent.args["y"] -= self.args["speed_y"] * 2

        return parent

class Rotate(Behavior):
    def active(self, parent, event):
        if event == None:
            pass
        else:
            if event.keysym == self.args["rotation_left"]:
                parent.args["angle"] -= self.args["rotation_speed"]
            if event.keysym == self.args["rotation_right"]:
                parent.args["angle"] += self.args["rotation_speed"]
        return parent

class Visible(Behavior):
    def active(self, parent, event):
        img2 = Image.open(parent.args["image"])
        img_photo2 = ImageTk.PhotoImage(img2)
        label = tkinter.Label(parent.args["screen"], image=img_photo2)
        self.args["image"].place(x = parent.args["x"], y = parent.args["y"], width = parent.args["width"], height = parent.args["height"])
        return parent
