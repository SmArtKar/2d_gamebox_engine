from tkinter import *
from objects import *
from behaviors import *
import time


root = Tk()

sprite = Sprite( {"solid":True, "x":100, "y":50, "speed_x":0, "speed_y":0, "screen":root, "angle":0, "width":50, "height":50, "image":"img.png", "behaviors":[Visible({}), Rotate({"rotation_left":"1", "rotation_right":"2", "rotation_speed":2}),  AdvancedMove({"friction":0.05, "up":"w", "down":"s", "left":"a", "right":"d", "speed_x":1, "speed_y":1, "speed_x_max":4, "speed_y_max":4}) ] } )

sprite2 = Sprite( {"solid":True, "x":110, "y":50, "speed_x":0, "speed_y":0, "screen":root, "angle":0, "width":50, "height":50, "image":"img2.png", "behaviors":[Visible({}), AdvancedMove({"friction":0.2, "up":"Up", "down":"Down", "left":"Left", "right":"Right", "speed_x":1, "speed_y":1, "speed_x_max":4, "speed_y_max":4}) ] } )

sprites = []

canvases = {}

def Pressing(event):
    for spriter in sprites:
        spriter.action_event(event)

sprites.append(sprite)

sprites.append(sprite2)

root.bind('<Key>', Pressing)

while True:
    for object in sprites:
        object.args["all_sprites"] = sprites
        object.action()
    root.update()
    time.sleep(0.02)
