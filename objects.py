from tkinter import *
from PIL import *
from PIL import Image, ImageTk

class Object():
    def __init__(self, args):
        self.args = args

class Sprite(Object):
    def action(self):
        for behavior in self.args["behaviors"]:
            self = behavior.active(self, None)
        return self.args["image"]

    def action_event(self, event):
        for behavior in self.args["behaviors"]:
            self = behavior.active(self, event)